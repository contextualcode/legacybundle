<?php

namespace ThinkCreative\LegacyBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class LegacyCompilerPass implements CompilerPassInterface
{

    public function process(ContainerBuilder $container) {
        if(
            $container->hasDefinition('thinkcreative.settingsinjection')
        ) {
            $SettingsInjection = $container->getDefinition('thinkcreative.settingsinjection');
            $SettingsInjectionServices = $container->findTaggedServiceIds('thinkcreative.settingsinjection');

            foreach ($SettingsInjectionServices as $ID => $Attributes) {
                $SettingsInjection->addMethodCall(
                    'addInjectionHandler', array( new Reference($ID) )
                );
            }
        }
    }

}
