<?php

namespace ThinkCreative\LegacyBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use ThinkCreative\LegacyBundle\DependencyInjection\Compiler\LegacyCompilerPass;

class ThinkCreativeLegacyBundle extends Bundle
{

    protected $name = "ThinkCreativeLegacyBundle";

    public function build(ContainerBuilder $container) {
        parent::build($container);
        $container->addCompilerPass(
            new LegacyCompilerPass()
        );
    }

}
